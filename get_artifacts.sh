if ! [ -z "${JOB_REF}" ]; then
  OUT_FILE="artifacts.zip"
  echo "${JOB_REF}"
  echo "Wait a while until artifacts are uploaded"
  sleep 5m
  curl -L -o ${OUT_FILE} "${JOB_REF}/artifacts/download?"
  mkdir 'unzip'
  unzip ${OUT_FILE} -d 'unzip/'
  ls 'unzip/pipeline_cache'
  cp --verbose -r 'unzip/pipeline_cache/public/.' 'public/'
  rm -rf 'unzip'
fi
