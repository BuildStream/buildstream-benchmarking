#!/usr/bin/python3

#
#  Copyright (C) 2018 Codethink Limited
#  Copyright (C) 2018 Bloomberg Finance LP
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Lachlan Mackenzie <lachlan.mackenzie@codethink.co.uk>

import argparse
import os
import tempfile
import shutil
import time
from distutils.file_util import copy_file

def main():

   output_path = os.path.join('public', 'index.html')
   input_path = 'public'
   directories = list()
   directory = 'public'
   parser = argparse.ArgumentParser()
   parser.add_argument("-d", "--sub_page_directory",
                       help="Directory containing html subdirectories each with index.html",
                       type=str)
   parser.add_argument("-o", "--output_path",
                       help="Output path for master index file",
                       type=str)
   args = parser.parse_args()

   temp_staging_area = tempfile.mkdtemp(prefix='temp_staging_location')

   try:
      if bool(args.sub_page_directory):
         if os.path.isdir(args.sub_page_directory):
            directory = args.sub_page_directory
            print(directory)
         else:
            logging.error("Specified html directory does not exist", args.sub_page_directory)
            sys.exit(1)

      if bool(args.output_path):
         output_path = args.output_path

      for entry in os.scandir(directory):
         if os.path.isdir(entry):
            print(entry)
            directories.append(entry)

      # Generate index html page
      index_template_page_path = 'index_page.html'
      index_page_path = os.path.join(temp_staging_area, 'index_page.html')
      shutil.copyfile(index_template_page_path, index_page_path)
      with open(index_page_path, 'r') as f:
         page = f.readlines()

      new_file = ""
      for line in page:
         if 'LINK_REF_STUB' in line:
            for entry in directories:
               entry_path = os.path.join(entry.name, 'public', 'index_page.html')
               new_file += line.replace("LINK_REF_STUB", entry_path).replace("Link_Stub", entry.name)
         elif 'Title_Stub' in line:
            index_page_title = 'Results: ' + time.strftime("%Y%m%d-%H%M%S")
            new_file += line.replace('Title_Stub', index_page_title)
         else:
            new_file += line

      with open(index_page_path, 'w') as f:
         f.write(new_file)

      # Copy files to destination
      try:
         copy_file(index_page_path, output_path)
      except OSError as err:
         logging.error("Unable to copy pages to target: ", args.output_path)
         sys.exit(1)

   finally:
      shutil.rmtree(temp_staging_area)


if __name__ == "__main__":
   main()
   

